// loggingservice.js
const fs = require("fs");
const redis = require("redis");

const client = redis.createClient();

// Підписка на повідомлення про створення користувача
client.on("message", (channel, message) => {
  if (channel === "user_created") {
    logUserCreation(JSON.parse(message));
  }
});

function logUserCreation(userData) {
  const logData = `${new Date().toISOString()} - New user created: ${
    userData.userName
  } (${userData.userEmail})\n`;
  fs.appendFile("user_creation_logs.txt", logData, (err) => {
    if (err) {
      console.error("Error logging user creation:", err);
    }
  });
}

client.subscribe("user_created");

console.log("Logging service is running");
