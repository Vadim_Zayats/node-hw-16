const express = require("express");
const bodyParser = require("body-parser");
const redis = require("redis");

const app = express();
const client = redis.createClient();
app.use(bodyParser.json());

app.post("/users/create", (req, res) => {
  const { name, email } = req.body;
  const userId = Math.floor(Math.random() * 100000);
  client.hSet(
    "users",
    userId,
    JSON.stringify({ userEmail: email, userName: name })
  );
  client.publish(
    "user_created",
    JSON.stringify({ userEmail: email, userName: name })
  );
  res.json("user created successfully");
});

app.get("/users/:userId", async (req, res) => {
  const userId = req.params.userId;
  const user = await client.hGet("users", userId);
  res.status(200).json(JSON.parse(user));
});

app.listen(3000, async () => {
  await client.connect();

  client.on("error", (err) => {
    console.error(`Redis client not connected: ${err}`);
  });

  console.log("Cart service listening on port 3000");
});
